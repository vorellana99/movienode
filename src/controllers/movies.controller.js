const exp = {};
const axios = require('axios');
const Movies = require('../models/movies')
const config = require('../config');

exp.searchMovies = async (ctx, next) => {
    const year = ctx.headers.year? '&y=' + ctx.headers.year: '';
    const search = ctx.request.query.title;
    const url = `${config.urlMovies}?apikey=${config.apiKey}&t=${search}${year}`
    let res = await axios.get(url);
    const imdbID = res.data.imdbID;
    if (res.data.Response === 'True'){
        const title = res.data.Title;
        const movies = await Movies.find({imdbID});
        let resSave = {};
        let message = `La película <${title}> se registró correctamente en nuestra Base de Datos.`;
        if (movies.length === 0){
            const newMovie = new Movies(res.data);
            resSave = await newMovie.save();
            ctx.status = 201;
        } else {
            message = `La película <${title}> ya existe en nuestra Base de Datos.`;
            ctx.status = 202;
        }
        ctx.body = {
            success: true,
            message,
            data: resSave._doc
        }
    } else {
        ctx.status = 404;
        ctx.body = {
            success: false,
            message: 'No se encontró la película.'
        }
    }
    await next();
}

exp.getMovies = async (ctx, next) => {
    const page = ctx.headers.page;
    const options = {
        page: page,
        limit: 5,
    };
    // const movies = await Movies.find({});
    const resPag = await Movies.paginate({}, options)
    const movies = resPag.docs.map(d => {
        obj = { 
            Title: d.Title, 
            Year:d.Year, 
            Released: d.Released, 
            Genre: d.Genre,
            Director: d.Director, 
            Actors: d.Actors,
            Plot: d.Plot, 
            imdbID: d.imdbID 
        }
        return obj;
    })
    ctx.status = 200;
    ctx.body = {
        success: true,
        message: '-',
        count: resPag.totalDocs,
        pages: resPag.totalPages,
        page: resPag.page,
        limit: resPag.limit,
        data: movies
    }
    await next();
} 

exp.replaceInMovie = async (ctx, next) => {
    const {movie, find, replace} = ctx.request.body
    const movies = await Movies.find({Title: movie});
    let responseMovies = [];
    if (movies.length > 0){
        for(item of movies){
            let re = new RegExp(find, 'g');
            const prevPlot = item.Plot;
            let plot = item.Plot;
            plot = plot.replace(re, replace);
            const filter = {imdbID: item.imdbID}
            const update = { Plot: plot}
            let doc = await Movies.findOneAndUpdate(filter, update)
            responseMovies.push({
                imdbID: item.imdbID,
                Title: item.Title,
                previousPlot: prevPlot,
                currentPlot: plot
            })
        }
        ctx.status = 200;
        ctx.body = {
            success: true,
            message: `Se realizó el reemplazo en las películas con el título <${movie}> en nuestra Base de Datos.`,
            data: responseMovies
        }        
    } else {
        ctx.status = 404;
        ctx.body = {
            success: false,
            message: `No se encontró película alguna con el título <${movie}> en nuestra Base de Datos.`
        }
    }
    await next();
}

module.exports = exp;