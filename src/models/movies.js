const { Schema, model } = require('mongoose');
const mongoosePagination = require('mongoose-paginate-v2');

const MoviesSchema = new Schema({
    id: {
        type: Schema.Types.ObjectId,
        index: true,
        require: true,
        auto: true
    },
    Title: String,
    Year: String, 
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    imdbID: String,
    Ratings: {
        Source: String,
        Value: String
    }
})

MoviesSchema.plugin(mongoosePagination);

module.exports = model('movies', MoviesSchema);