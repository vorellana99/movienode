const Koa = require('koa');
const err = require('./helpers/error');
const {routes, allowedMethods} = require('./routes');
const koaSwagger = require('koa2-swagger-ui');
const bodyParser = require('koa-body-parser');
const {inputValidation} = require('./helpers/input-validation');
const logger = require('./helpers/logger');
const {startDatabase} = require('./managers/db-manager');
const yamljs = require('yamljs');
const spec = yamljs.load('./openapi.yaml');
const cors = require('@koa/cors');

const app = new Koa();

// ***** connection to MongoDB *****
(async () => {
  await startDatabase();
})();

// ***** setup cors *****
app.use(cors());

// ***** error handler *****
app.use(err);

// ***** stablishing swagger *****
app.use(
  koaSwagger({
    routePrefix: '/', // '/swagger'
    swaggerOptions: {spec}
  }),
);

// ***** set body parser *****
app.use(bodyParser());

// ***** validation with Joi *****
app.use(async (ctx, next) => {
  logger.info(`Request ${ctx.method} Path: ${ctx.URL.pathname}`);
  await inputValidation(ctx);
  await next();
});

// ***** set routes *****
app
  .use(routes())
  .use(allowedMethods());

// ***** start server *****
const server = app.listen(3000, function () {
  const host = server.address().address;
  const port = server.address().port;
  logger.info(`Iniciando el servidor en: ${host}:${port}`)
});

module.exports = server;