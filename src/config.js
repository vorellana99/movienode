require('dotenv').config()

module.exports = {
    secret : process.env.NODE_SECRET,
    port: process.env.PORT || 3000 ,
    connectionDb: process.env.NODE_CONNECT_DB || 'mongodb+srv://vorellana:1a2s3d4F5G6H$@cluster0.yvezp.mongodb.net/movie-node-db',
    apiKey: process.env.NODE_API_KEY || 'c79e84e4',
    urlMovies: process.env.NODE_URL_MOVIES || 'https://www.omdbapi.com/'
};