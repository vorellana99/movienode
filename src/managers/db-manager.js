exp = {}
const mongoose = require('mongoose');
const config = require('../config');

exp.startDatabase = async () => {
    const strConnection = config.connectionDb;
    mongoose.connect(strConnection, {
    useNewUrlParser: true, 
    useUnifiedTopology: true
    }, (err) => {
    if (err) {
        console.log("**** Failed DB Connection ****");
        console.log(err);
    } else {
        console.log("**** Successful DB connection ****");
    }
    })
}

module.exports = exp;