const exp = {};
const Joi = require('joi');
const moviePath = '/movies';

searchSchema = Joi.object({
    title: Joi.string().trim().required(),
    year: Joi.string().trim()
})

replacementSchema = Joi.object({
    find: Joi.string().trim().required(),
    movie: Joi.string().trim().required(),
    replace: Joi.string().trim().required()
})

exp.inputValidation = async (ctx) => {
    const path = ctx.URL.pathname;
    if(ctx.method === 'GET'){
        if(path === moviePath) {

        } else if(path === moviePath + '/search') {
            const objValidate = {
                title: ctx.request.query.title,
                year: ctx.headers.year
            }
            await searchSchema.validateAsync(objValidate);
        }
    } else if(ctx.method === 'POST'){
        if(path === moviePath + '/replace'){
            await replacementSchema.validateAsync(ctx.request.body);
        }
    }
}

module.exports = exp;