const Router = require('koa-router');
const { searchMovies, getMovies, replaceInMovie } = require('../controllers/movies.controller');
const router = new Router();
const moviePath = '/movies';

router
    .get(moviePath + '/search', searchMovies)
    .get(moviePath, getMovies)
    .post(moviePath + '/replace', replaceInMovie);
    
module.exports = {
    routes() { return router.routes() }, 
    allowedMethods () { return router.allowedMethods() }
}