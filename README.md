# MoviesNode - Operaciones sobre películas

### Desarrollo de una API REST con operaciones de Películas: Buscar, Listar y Reemplazar.
### Desarrollado principalmente con **Node.js, Koa**, desplegado/publicado en **Azure** y con configuración para despliegue en **Docker**.

### La aplicación se encuentra desplegada en el servicio de **Azure** y puede acceder inmediatamente a su documentación con **Swagger** en el siguiente enlace:
### https://movienode-backend.azurewebsites.net/swagger

![alt text](https://gitlab.com/vorellana99/movienode/-/raw/main/resources/backend-1.png)

![alt text](https://gitlab.com/vorellana99/movienode/-/raw/main/resources/backend-2.png)

## 1. Operaciones sobre Películas

*  **Buscador de películas:** Busca una película utilizando la API de **omdbapi** y si no existe la inserta en una Base de Datos propia.
*  **Obtener todas las películas:** Retorna una lista de todas las películas presentadas en bloques de 5 registros indicando con un parámetro el número de la página.
*  **Buscar y reemplazar:** Busca una película y reemplaza un texto en el campo **plot** de ésta.

## 2. Instalación y uso (Despliegue y pruebas unitarias)
**Es un requisito tener instalado docker y docker-compose.** Si no los tiene puede revisar los siguientes enlaces para la instalación:
#### https://docs.docker.com/get-docker
#### https://docs.docker.com/compose/install

```sh
# descargamos el proyecto
git clone https://gitlab.com/vorellana99/movienode.git

# entramos a la carpeta del proyecto
cd movienode

# iniciamos el despliegue del servicio de backend
docker-compose up -d 
```
Una vez finalizado el proceso de despliegue ya podemos utilizar la aplicación.
Ahora la API se encuentra funcionando en el puerto 3000 y ya podemos acceder mediante la url base:
```sh
# url
http://localhost:3000
```
### Ejecutar pruebas unitarias

Verificamos que estamos en la carpeta raíz del proyecto: "movienode"
```sh
# Ejecutamos el **test** para la pruebas unitarias con **jest**
npm run test
```
  ![alt text](![alt text](https://gitlab.com/vorellana99/movienode/-/raw/main/resources/backend-3.png))

## 3. Tecnologías de desarrollo
Para el presente proyecto se utilizarón las siguientes tecnologías como librerías, frameworks, servicios en la nube, herramientas de despliegue entre otros.

### Backend
*  **Node.js:** Entorno en tiempo de ejecución para desarrollar el Backend en Javascript.
* **Koa:** Framework e infraestructura web rápida, minimalista y flexible para Node.js. 
* **Nodemon:** Aplicación para reiniciar el servidor automáticamente después de cada cambio.
* **joi:** Lenguaje de descripción de esquemas para validar datos.
* **mongoose:** Librería para Node.js que nos permite utilizar una base de datos de MongooDB.
* **winston:** Librería para registrar por terminal y archivos Log las incidencias de la aplicación.
* **Axios:** Librería para realizar operaciones como cliente HTTP.
* **koa2-swagger-ui:** Librería para el uso de Swagger en Koa.

### Database
*  **MongoDB**: Base de datos NoSQL.
*  **MongoDB Atlas**: Servicio en la nube para alojar Base de datos MongoDB. Actualmente la BD del proyecto se encuentra desplegada en éste servicio.
### Testing
*  **Jest**: Framework de pruebas para Javascript.
*  **supertest**: Líbreria para comprobar las respuestas de las solicitudes HTTP.
### Deployment
*  **Azure**: Servicio en la nube que se utilizó para desplegar la aplicación.
*  **Docker:** Tecnología de contenedores que posibilita la creación y el uso de contenedores.
*  **Docker Compose:** Herramienta que permite simplificar el uso de Docker y gestionar varios contenedores.
*  **GitLab**: Servicio de repositorio de código fuente en donde se encuentran almacenado todo el código del proyecto.
## 4. Características
* Uso del framework **Koa**.
* Validación de entradas y headers a traves de un **Middleware** utilizando **joi**.
* Manejo de errores para todas las solicitudes HTTP con un **Middleware** antes de entrar a la lógica de negocio.
* Pruebas unitarias con **Jest**
* Registro de errores en un archivo Log utilizando **Winston**.
* Documentación con Swagger.
* Orquestación del despliegue en Docker con dockerfile y docker-compose.
* La estructura principal del proyecto se maneja en 2 capas: routes y controllers.
* La aplicación tiene implementada las 3 operaciones requerida en el documento de la prueba.
* Lista de todas las películas paginadas en bloques de 5 registros por página.
* Despliegue de la Aplicación Backend en Azure.
* Despliegue de la Base de Datos en MongoDB Atlas.

## Gracias
