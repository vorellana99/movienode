const request = require('supertest')
const app = require('../src/app')
const urlBase = '/movies';
// const urlLocation = urlBase + '/location';

describe('GET ' + urlBase + '/search', () => {
    test('should respond with a 202 status code', async () => {
        const response = await request(app).get(urlBase + '/search?title=300');
        expect(response.statusCode).toBe(202);
    })
    test('should respond with a json', async () => {
        const response = await request(app).get(urlBase + '/search?title=300');
        expect(response.body).toBeInstanceOf(Object);
    })
})

describe('GET ' + urlBase, () => {
    test('should respond with a 200 status code', async () => {
        const response = await request(app).get(urlBase);
        expect(response.statusCode).toBe(200);
    })
    test('should respond with a json', async () => {
        const response = await request(app).get(urlBase);
        expect(response.body).toBeInstanceOf(Object);
    })
})

describe('POST ' + urlBase + '/replace', () => {
    const body = {
        movie: "Tita",
        find: "a",
        replace: "4"
    }

    test('should respond with a 200 status code', async () => {
        const response = await request(app).post(urlBase + '/replace').send(body);
        expect(response.statusCode).toBe(200);
    })
    test('should respond with a json', async () => {
        const response = await request(app).post(urlBase + '/replace').send(body);
        expect(response.body).toBeInstanceOf(Object);
    })
})